#include "engine.h"
#include "required-service.h"

//RequiredService sample_service {"sample-service.dll"};

extern int xyz;
struct SpaceBarSubscriber : InputSubscriber
{
    SpaceBarSubscriber () : InputSubscriber(InputDef::KeyCode(SDLK_SPACE)) {}
    int presses = 0;
    void Execute () override
    {
        presses++;
		xyz =5;
        printf("space bar pressed %d time%s so far\n", presses, presses > 1 ? "s" : "");
		//RequiredService sample_service {"sample-service.dll"};
    }
}
SpaceBarSubscriberInstance;

struct VoluntaryThrower : InputSubscriber
{
    VoluntaryThrower () : InputSubscriber(InputDef::KeyCode(SDLK_x)) {}
    void Execute () override
    {
        printf("throwing exception voluntarily\n");
        throw 123;
    }
}
VoluntaryThrowerInstance;

struct SampleStartSubscriber : Subscriber
{
    SampleStartSubscriber () : Subscriber(&EngineInstance->GameStart) {}
    void Execute () override
    {
        printf("game starting\n");
    }
}
SampleStartSubscriberInstance;

struct SampleFrameSubscriber : Subscriber
{
    SampleFrameSubscriber () : Subscriber(&EngineInstance->GameFrame) {}
    void Execute () override
    {
        //printf("game frame\n");
    }
}
SampleFrameSubscriberInstance;

struct SampleClosingSubscriber : Subscriber
{
    SampleClosingSubscriber () : Subscriber(&EngineInstance->GameClosing) {}
    void Execute () override
    {
        printf("game closing\n");
    }
}
SampleClosingSubscriberInstance;
